const btn = document.querySelector(".btn");
const userLocation = document.querySelector("#location");
const getIp = async () => {
  let response = await fetch("https://api.ipify.org/?format=json");
  return await response.json();
};
const getLoc = async (ip) => {
    let url = `http://ip-api.com/json/?query=${ip}&fields=status,continent,country,regionName,city,district,query`
    let response = await fetch(url);
    return await response.json();
}
btn.addEventListener("click", async () => {
  let ip = await getIp();
  let location = await getLoc(ip);
  userLocation.innerHTML = `
    <p>Continent: ${location.continent}</p>
    <p>Country: ${location.country}</p>
    <p>Region name: ${location.regionName}</p>
    <p>City: ${location.city}</p>
    <p>District: ${location.district == "" ? "Not found" : location.district}</p>
  `
  console.log(location);
});
/*
city: "Kyiv"
continent: "Europe"
country: "Ukraine"
district: ""
query: "93.126.83.241"
regionName: "Kyiv City"
status: "success"
*/